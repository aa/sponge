from django.contrib.admin.apps import AdminConfig

class SpongeAdminConfig(AdminConfig):
    default_site = 'spongeproject.admin.SpongeAdminSite'