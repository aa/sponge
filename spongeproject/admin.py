from django.contrib import admin

class SpongeAdminSite(admin.AdminSite):
    site_title = "Sponge admin"
    site_header = "Sponge admin"