from django.contrib import admin
from .models import MimeType, URL, Response, PageContent, FileContent, Namespace, Scraper, Selector
from adminsortable2.admin import SortableAdminBase, SortableStackedInline, SortableTabularInline


@admin.register(MimeType)
class MimeTypeAdmin(admin.ModelAdmin):
    list_display = ('mimetype', 'extension', 'description')

@admin.register(URL)
class URLAdmin(admin.ModelAdmin):
    list_display = ('url', )
    list_filter = ('scheme', 'hostname')
    search_fields = ('hostname', 'path', 'query')

@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
    list_display = ('url', 'status', 'date', 'content_type', 'content_length', 'redirected_from')
    list_filter = ('status', 'url__hostname', 'content_type')
    date_hierarchy = 'date'
    # search_fields = ('hostname', 'path', 'query')
    raw_id_fields = ("url", "content", "file", "redirected_from")

@admin.register(PageContent)
class PageContentAdmin(admin.ModelAdmin):
    list_display = ('sha1',)
    # list_filter = ('status', 'url__hostname', 'content_type')
    # date_hierarchy = 'date'
    # search_fields = ('hostname', 'path', 'query')
    # raw_id_fields = ("url", "content", "file", "redirected_to")

@admin.register(FileContent)
class FileContentAdmin(admin.ModelAdmin):
    list_display = ('sha1', 'file', 'extension', 'size')
    date_hierarchy = "created"
    list_filter = ("extension", "created")






@admin.register(Namespace)
class NamespaceAdmin(admin.ModelAdmin):
    list_display = ('name', 'url')

class SelectorTabularInline(SortableTabularInline):
    model = Selector

class SelectorStackedInline(SortableStackedInline):
    model = Selector

@admin.register(Scraper)
class ScraperAdmin(SortableAdminBase, admin.ModelAdmin):
    list_display = ('name', 'match', 'recompose')
    inlines = [SelectorStackedInline]


# admin.site.register(Namespace, NamespaceAdmin)
# admin.site.register(Scraper)