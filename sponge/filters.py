from xml.etree import ElementTree as ET

# print([cls.__name__ for cls in Foo.__subclasses__()])

class ValueFilter (object):
    def __init__(self, verbose=False):
        self.verbose=verbose

    def filter(elt):
        pass

from dateutil.parser import parse as parse_datetime, ParserError
import daterangeparser, pyparsing
from .utils import textContent, bytwo

def get_value_filter (name, verbose=False):
    for cls in ValueFilter.__subclasses__():
        if cls.__name__.lower() == name.lower():
            return cls(verbose)
        
        
class Autolink (ValueFilter):
    def filter (self, elt):
        """asserts len(elt) == 0: throws exception if there's non-textual content ... todo: adapt for all cases"""
        assert len(elt) == 0
        split = re.split(r"(https?:\/\/\S+)", elt.text)
        # print (f"split ({len(split)}) : {split}")
        elt.text = split[0]
        for link, tail in bytwo(split[1:]):
            # print (link, tail)
            a = ET.SubElement(elt, "a")
            a.attrib['href'] = link
            a.tail = tail

class Datetime (ValueFilter):
    def filter (self, elt):
        if self.verbose:
            tag_contents = ET.tostring(elt, method="html", encoding="unicode")
            print (f"datetime_filter: {tag_contents}")
        date = None
        tc = textContent(elt)
        try:
            date_range = daterangeparser.parse(tc)
            date = date_range[0]
            if self.verbose:
                print (f"daterangeparser.parse {tc}, {date}")
        except pyparsing.ParseException:
            # print ("daterangeparser failed")
            try:
                # attempt to parse the time tag contents (event date)
                # 2021-05-30: adding ignoretz as the default seems (locally) to be way off (what's the default?)
                # eventually this should be customizable (set a default timezone ?!)
                date = parse_datetime(tc, ignoretz=True)
                # print ("parse_datetime (tc)", textContent(time_tag), date)
            except ParserError:
                try:
                    # else datetime attribute has publication date
                    date = parse_datetime(elt.attrib.get("datetime"))
                    # print ("parse_datetime (attrib)", time_tag.attrib.get("datetime"), date)
                except ParserError:
                    date = None
        if date is not None:
            content = date.isoformat()
            if self.verbose:
                print (f"Setting datetime attribute to {content}")
            # REPLACE the given datetime attribute ?!
            elt.attrib['datetime'] = content
            # g.add((subj, CONSTANT.date, Literal(date.isoformat())))

        
from urllib.parse import unquote as urlunquote
import re

# todo filters should have more context (like the full url of the current page oa) + tree of the page (to for instance get the lang from the html tag)

def constant_ensure_lang (href, lang):
    m = re.search(r"^(?P<base_url>.+?)\?lang=(?P<lang>\w+)$", href)
    if m is not None:
        d = m.groupdict()
        xlang = d.get('lang')
        if xlang != lang:
            return d.get("base_url")+f"?lang={lang}"
        else:
            return href
    else:
        return href+f"?lang={lang}"

class ConstantConverser (ValueFilter):
    
    def filter (self, elt):
        """ resolves (partial) urls like:
        Constant_V-Rekenmatig-Arabisch.html?lang=en&action=converser&var_lang=fr&redirect=https%3A%2F%2Fconstantvzw.org%2Fsite%2FConstant_V-L-Arabe-Numerique.html
        to:
        """
        href = elt.attrib.get("href")
        print (f"constant_converser_filter {href}")
        m = re.search(r"action=converser&var_lang=(?P<var_lang>\w+)&redirect=(?P<redirect>.*)$", href)
        if m is not None:
            d = m.groupdict()
            new_href = urlunquote(d['redirect'])
            new_href = constant_ensure_lang(new_href, d['var_lang'])
            print (f"constant_converser_filter: rewriting href {href} = {new_href}")
            elt.attrib["href"] = new_href

