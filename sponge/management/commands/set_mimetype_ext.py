from django.core.management.base import BaseCommand, CommandError
from sponge.models import MimeType, Response
from django.conf import settings
import os


class Command(BaseCommand):
    help = ''

    def add_arguments(self, parser):
        parser.add_argument('mimetype')
        parser.add_argument('extension')

    def handle(self, *args, **options):
        mt, created = MimeType.objects.get_or_create(mimetype=options['mimetype'])
        mt.extension = ext = options['extension']
        mt.save()
        # rename any filecontent that has the data extesion
        for response in Response.objects.filter(content_type=mt.mimetype):
            if response.file:
                if True or str(response.file.file).endswith(".data"):
                    oldpath = response.file.file.path
                    newpath = oldpath[:-5] + f".{ext}"
                    os.rename(oldpath, newpath)
                    newrelpath = os.path.relpath(newpath, settings.MEDIA_ROOT)
                    print (f"{oldpath}=>{newpath} ({newrelpath})")
                    response.file.file = newrelpath
                    response.file.extension = ext
                    response.file.save()


