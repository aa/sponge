from django.core.management.base import BaseCommand, CommandError
from sponge.models import Namespace, Scraper, Selector, Append, Wrap
from ruamel.yaml import YAML


class Command(BaseCommand):
    help = 'init scrapers based on a yaml file'

    def add_arguments(self, parser):
        parser.add_argument('path')
        parser.add_argument('--deleteall', action="store_true", default=False)

    def handle(self, *args, **options):
        if options['deleteall']:
            Scraper.objects.all().delete()
        yaml = YAML()
        with open (options['path']) as fin:
            items = yaml.load_all(fin)
            for scraper_info in items:
                if scraper_info is not None:
                    scraper = Scraper()
                    if 'name' in scraper_info:
                        scraper.name = scraper_info['name']
                        print (f"scraper name:{scraper.name}")
                    if 'match' in scraper_info:
                        scraper.match = scraper_info['match']
                    if 'recompose' in scraper_info:
                        scraper.match = scraper_info['recompose']
                    if 'canonical' in scraper_info:
                        scraper.match = scraper_info['canonical']
                    if 'prefixes' in scraper_info:
                        for key,value in scraper_info['prefixes'].items():
                            ns, ns_created = Namespace.objects.get_or_create(
                                name=key,
                                url=value
                            )
                    scraper.save()
                    if 'selectors' in scraper_info:
                        for selector_info in scraper_info['selectors']:
                            selector = Selector(scraper=scraper)
                            selector.selector = selector_info['selector']
                            if 'property' in selector_info:
                                selector.property = selector_info['property']
                            if 'typeof' in selector_info:
                                selector.typeof = selector_info['typeof']
                            if 'filters' in selector_info:
                                selector.filters = selector_info['filters']
                            selector.save()
                            if 'wrap' in selector_info:
                                for wrap_info in selector_info['wrap']:
                                    wrap = Wrap(selector=selector)
                                    if 'tag' in wrap_info:
                                        wrap.tag = wrap_info['tag']
                                    if 'property' in wrap_info:
                                        wrap.property = wrap_info['property']
                                    wrap.save()

                            if 'append' in selector_info:
                                for append_info in selector_info['append']:
                                    print (f"append_info: {append_info}")
                                    append = Append(selector=selector)
                                    if 'property' in append_info:
                                        append.property = append_info['property']
                                    if 'value' in append_info:
                                        append.value = append_info['value']
                                    if 'subject' in append_info:
                                        append.value = append_info['subject']
                                    if 'datatype' in append_info:
                                        append.datatype = append_info['datatype']
                                    
            # self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))