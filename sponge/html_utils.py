from .etree_utils import *
import html5lib
import re
# from urllib.parse import urljoin


def filter_html (src, url_filter):
    t = html5lib.parse(src, namespaceHTMLElements=False)
    tree_filter_linked_urls(t, url_filter)
    return outerHTML(t)

def tree_filter_linked_urls (t, url_filter):
    count = 0
    for href_elt in t.findall(".//*[@href]"):
        href_elt.attrib['href'] = url_filter(href_elt.attrib.get("href"))
        count += 1
    for src_elt in t.findall(".//*[@src]"):
        src_elt.attrib['src'] = url_filter(src_elt.attrib.get("src"))
        count += 1
    for style in t.findall(".//style"):
        style_src = filter_urls_in_css_src(innerHTML(style), url_filter)
        setInnerHTML(style, style_src)
        count += 1
    return count

def filter_urls_in_css_src(src:str, url_filter):
    """
    src: CSS source (str)
    url: of processing context, for url relativation
    url_filter: function applied to each url

    avoids data: urls
    This function used to take url as a parameter, now url_filter is a lambda of type (str)=>str
    Also, removed urlquote, that seems weird here ?!
    """
    def css_sub(m):
        # href = urljoin(url, m.group(2))
        href = m.group(2)
        if not href.startswith("data"): # True or self.should_localize(href):
            href, fragment = split_fragment(href) # TESTING
            # return "url("+m.group(1)+urlquote(url_filter(href))+m.group(3)+fragment+")"
            return "url("+m.group(1)+url_filter(href)+m.group(3)+fragment+")"
        return m.group(0)                            
    return re.sub(r"""url\((['" ]*)(.+?)(['" ]*)\)""", css_sub, src)
