from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
from django.core.paginator import Paginator
from urllib.parse import urlparse, urljoin
import html5lib
# import urllib.request
# import requests
from .models import URL, Response, PageContent
from .etree_utils import *
from .filters import *


def index(request):
    return HttpResponse("Hello, world. You're at the sponge index.")

def get_url_from_request (request, path=None):
    if path is not None:
        url = path
        if request.META.get("QUERY_STRING"):
            path += "?"+request.META.get("QUERY_STRING")
    if request.method == "POST":
        post = request.POST
        url = post.get("url").strip()
    else:
        url = path
    return url

def browse(request, path=None):
    ctx = {}
    url_request_headers = {}
    # ctx['url'] = URL.parse(get_url_from_request(request, path))
    url = get_url_from_request(request, path)
        
    if url:
        ctx['url'], _ = URL.get_or_create(url)
        if settings.USER_AGENT:
            request_headers = {'User-Agent': settings.USER_AGENT}
        else:
            request_headers = None
        if request.method == "POST" and request.POST.get("_submit", "") == "reload":
            ctx['response'] = response = ctx['url'].get(request_headers)
        else:
            ctx['response'] = response = ctx['url'].cget(request_headers)
        # response.save_contents()
        # update to possibly redirected response url
        ctx['url'] = ctx['response'].url
    return render(request, "sponge/browse.html", ctx)

from .html_utils import filter_html, filter_urls_in_css_src, tree_filter_linked_urls

# def make_link_absolutizer (baseurl):
#     return lambda url: urljoin(baseurl, url)

def absolutize_url(baseurl, url):
    return urljoin(baseurl, url)

def localize_url (url):
    return f"/c/{url}"

# conditional_get
# NEED CACHED URL_GET => RESPONSE

def content(request, path=None):
    ctx = {}
    url_request_headers = {}
    # ctx['url'] = URL.parse(get_url_from_request(request, path))
    ctx['url'], _ = URL.get_or_create(get_url_from_request(request, path))
    ctx['response'] = response = ctx['url'].cget()
    # response.save_contents()
    # IF html....
    ct = response.content_type

    if response.content_type == "text/html":
        src = filter_html(response.content.text, lambda url: localize_url(absolutize_url(response.url.url(), url)))
        return HttpResponse(src, content_type=response.content_type_with_encoding)
    elif response.content_type == "text/css":
        with open(response.file.file.path) as fin:
            text = fin.read()
        src = filter_urls_in_css_src(text, lambda url: localize_url(absolutize_url(response.url.url(), url)))
        return HttpResponse(src, content_type=response.content_type_with_encoding)
    elif response.file:
        return HttpResponseRedirect(response.file.file.url)
    # return render(request, "sponge/browse.html", ctx)

# Would it be useful to have a session structure to group requests...

def content_by_id (request, content_id):
    content = get_object_or_404(PageContent, pk=content_id)
    # return HttpResponse(content.text)
    responses = content.responses.order_by("-date")
    if responses.count():
        last_response = content.responses.order_by("-date")[0]
        src = filter_html(content.text, lambda url: localize_url(absolutize_url(last_response.url.url(), url)))
    else:
        src = content.text
    return HttpResponse(src)

# import django_filters

# class ResponseFilter(django_filters.FilterSet):
#     class Meta:
#         model = Response
#         fields = ['url__hostname']

def responses (request):
    ctx = {}
    ctx['filter_params'] = filter_params = {}

    filter_hostname = request.GET.get("hostname")
    filter_content_type = request.GET.get("content_type")

    responses_q = Response.objects.all()
    if filter_hostname is not None:
        filter_params['hostname'] = filter_hostname
        responses_q = responses_q.filter(url__hostname=filter_hostname)
    if filter_content_type is not None:
        filter_params['content_type'] = filter_content_type
        responses_q = responses_q.filter(content_type=filter_content_type)

    r_hostnames = Response.objects.order_by("url__hostname").values_list("url__hostname", flat=True).distinct()
    ctx['hostnames'] = r_hostnames

    r_content_types = Response.objects.order_by("content_type").values_list("content_type", flat=True).distinct()
    ctx['content_types'] = r_content_types

    # ctx['f'] = f = ResponseFilter(request.GET, queryset=Response.objects.all())
    ctx['responses'] = responses_q
    # responses_q = responses_q.values("url").distinct()
    responses = Paginator(responses_q, 25)
    
    page_number = request.GET.get('page')
    ctx['page'] = responses.get_page(page_number)

    return render(request, "sponge/responses.html", ctx)
