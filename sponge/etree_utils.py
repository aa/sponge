from xml.etree import ElementTree as ET

def innerHTML (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="html", encoding="unicode") for x in elt])

def outerHTML (elt):
    return ET.tostring(elt, method="html", encoding="unicode")

def setInnerHTML (elt, text):
    contents = list(elt)
    for c in contents:
        elt.remove(c)
    elt.text = text

def split_fragment(href):
    try:
        ri = href.rindex("#")
        return href[:ri], href[ri:]
    except ValueError:
        return href, ''

def split_query(href):
    try:
        ri = href.rindex("?")
        return href[:ri], href[ri:]
    except ValueError:
        return href, ''
