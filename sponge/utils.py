import rdflib
from xml.etree import ElementTree as ET
import re
from cssselect import HTMLTranslator
import elementpath

# from ruamel.yaml.comments import CommentedMap

def querySelector (t, selector):
    for elt in elementpath.select(t, HTMLTranslator().css_to_xpath(selector)):
        return elt

def querySelectorAll (t, selector):
    for elt in elementpath.select(t, HTMLTranslator().css_to_xpath(selector)):
        yield elt

def bytwo (items):
    # simplification of grouper given here: https://docs.python.org/3/library/itertools.html
    args = [iter(items)] * 2
    return zip(*args)

def pluralize (thing):
    if type(thing) == list:
        for item in thing:
            yield item
    else:
        yield thing
    # elif type(thing) == dict:
    #     yield thing
    # elif type(thing) == CommentedMap:
    #     yield thing
    # else:

def parse_prefix (v):
    p = {}
    for m in re.finditer(r"(\w+): *(\S+)", v):
        key, value = m.groups()
        p[key] = value
    return p

def textContent (elt):
    if elt.text != None:
        ret = elt.text
    else:
        ret = u""
    return ret + u"".join([ET.tostring(x, method="text", encoding="utf8").decode("utf-8") for x in elt])

def etree_wrap(elt, newelt, **props):
    children = []
    # print ("first loop")
    for c in elt:
        # print (f"c {c}")
        children.append(c)
    # print ("second loop")
    for c in children:
        # print (f"c {c}")
        elt.remove(c)
    subelt = ET.SubElement(elt, newelt, **props)
    for c in children:
        subelt.append(c)
    if elt.text is not None:
        subelt.text = elt.text
        elt.text = None

def remove_prediates_by_namespaces (g, remove_namespaces):
    """ remove predicates matching given namespaces """
    outg = rdflib.Graph()
    for (s, p, o) in g:
        skip = False
        if isinstance(p, rdflib.URIRef):
            for r in remove_namespaces:
                if str(p).startswith(r):
                    skip = True
                    break
                    # print ("REMOVE", p)
        if not skip:
            outg.add((s, p, o))
    return outg

def filter_predicates_by_namespaces (g, filter_namespaces):
    """ only let predicates of the give namespaces through """
    outg = rdflib.Graph()
    for (s, p, o) in g:
        include = False
        if isinstance(p, rdflib.URIRef):
            for r in filter_namespaces:
                if str(p).startswith(r):
                    #print (f"match {r}")
                    include = True
                    break
                #else:
                #    print (f"reject {r}")
        if include:
            outg.add((s, p, o))
    # print (f"filter {len(g)}->{len(outg)} by {len(filter_namespaces)} ns ")
    return outg
