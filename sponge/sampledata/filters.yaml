name: remove_canonical_self
comment: remove constant:canonical to self
update: |
    DELETE { ?s constant:canonical ?s }
    WHERE {
      ?s constant:canonical ?s .
    }
---
name: rewrite_has_part
comment: rewrite hasPart to isPartOf
update: | 
    DELETE { ?whole dcterms:hasPart ?part }
    INSERT { ?part dcterms:isPartOf ?whole }
    WHERE {
      ?whole dcterms:hasPart ?part . 
    }
---
name: rewrite_has_format
comment: rewrite hasFormat to isFormatOf
update: | 
    DELETE { ?original dcterms:hasFormat ?format }
    INSERT { ?format dcterms:isFormatOf ?original }
    WHERE {
      ?original dcterms:hasFormat ?format . 
    }
---
name: rewrite_has_tag
comment: rewrite hasTag to isTagOf
update: | 
    DELETE { ?tagged dcterms:hasTag ?tag }
    INSERT { ?tag dcterms:isTagOf ?tagged }
    WHERE {
      ?tagged dcterms:hasTag ?tag . 
    }
---
name: flip_rels
do:
  - rewrite_has_part
  - rewrite_has_format
  - rewrite_has_tag
---
name: resolve_is_format_of_in_objects
comment: resolve isFormatOf in objects
update: |
    DELETE { ?x ?p ?old }
    INSERT { ?x ?p ?new }
    WHERE {
      ?x ?p ?old . 
      ?old dcterms:isFormatOf ?new .
    }
---
name: resolve_is_format_of_in_subjects
comment: resolve isFormatOf in subjects
update: |
    DELETE { ?old ?p ?x }
    INSERT { ?new ?p ?x }
    WHERE {
      ?old ?p ?x . 
      ?old dcterms:isFormatOf ?new .
    }
---
name: gather_formats
comment: write all statements based on format (too much?)
do:
  - resolve_is_format_of_in_subjects
  - resolve_is_format_of_in_objects
---
name: rewrite_canonical_to_is_format_of
comment: rewrite constant:canonical to isFormatOf
update: | 
    INSERT { ?format dcterms:isFormatOf ?original }
    WHERE {
      ?format constant:canonical ?original . 
    }
---
name: resolve_redirections_in_objects
comment: resolve redirections in objects
update: |
    DELETE { ?x ?p ?old }
    INSERT { ?x ?p ?new }
    WHERE {
      ?x ?p ?old . 
      ?old http:redirectsTo ?new .
    }
---
name: resolve_redirections_in_subjects
comment: resolve redirections in subjects
update: |
    DELETE { ?old ?p ?x }
    INSERT { ?new ?p ?x }
    WHERE {
      ?old ?p ?x FILTER ( ?p != http:redirectsTo ) . 
      ?old http:redirectsTo ?new .
    }
---
name: gather_titles
comment: gather titles on (isFormatOf) original
update: | 
    INSERT {
      ?original dcterms:title ?title
    }
    WHERE {
      ?x dcterms:title ?title .
      ?x dcterms:isFormatOf ?original.
    }
---
name: inherit_is_part_of_as_subject
comment: Inherit isPartOf (as subject) through format
update: | 
    INSERT {
      ?original dcterms:isPartOf ?whole
    }
    WHERE {
      ?format dcterms:isFormatOf ?original .
      ?format dcterms:isPartOf ?whole .
    }
---
name: inherit_is_part_of_as_object
comment: Inherit isPartOf (as object) through format
update: | 
    INSERT {
      ?part dcterms:isPartOf ?original
    }
    WHERE {
      ?format dcterms:isFormatOf ?original .
      ?part dcterms:isPartOf ?format .
    }
---
name: inherit_is_tag_of_as_subject
comment: Inherit isTagOf (as subject) through format
update: | 
    INSERT {
      ?original dcterms:isTagOf ?tagged
    }
    WHERE {
      ?format dcterms:isFormatOf ?original .
      ?format dcterms:isTagOf ?tagged .
    }
---
name: inherit_is_tag_of_as_object
comment: Inherit isTagOf (as object) through format
update: | 
    INSERT {
      ?tag dcterms:isTagOf ?original
    }
    WHERE {
      ?format dcterms:isFormatOf ?original .
      ?tag dcterms:isTagOf ?format .
    }
---
name: crawl_all_matches
comment: request scrape on all subjects that matchedScraper
update: |
  PREFIX dcterms: <http://purl.org/dc/terms/>
  PREFIX sponge: <http://constantvzw.org/sponge/terms/>
  PREFIX aa: <http://activearchives.org/terms/>
  INSERT { ?s sponge:scrapeRequested true }
  WHERE {
    ?s sponge:matchedScraper ?scraper 
    FILTER NOT EXISTS {
     ?s sponge:scrapeRequested ?ts .
    }
  }
---
name: crawl_parts
do:
  - flip_rels
  - mark_parts_part
  - mark_parts_whole
---
name: crawl_formats
do:
  - flip_rels
  - mark_formats_format
  - mark_formats_original
---
name: crawl_custom
do:
  - remove_canonical_self
  - rewrite_canonical_to_is_format_of
  - flip_rels
  - mark_formats_format
  - mark_formats_original
  - mark_parts_part
  - mark_parts_whole
  - mark_redirects
---
name: expand_formats
update: |
  INSERT {
    ?format sponge:scrapeRequested true.
  }
  WHERE {
    ?format dcterms:isFormatOf ?original 
    FILTER NOT EXISTS {
     ?format sponge:scrapeRequested true .
    }
  }
---
name: mark_formats_format
update: |
  INSERT { ?format sponge:scrapeRequested true }
  WHERE {
    ?format dcterms:isFormatOf ?original 
    FILTER NOT EXISTS {
     ?format sponge:scrapeRequested true .
    }
  }
---
name: mark_formats_original
update: |
  INSERT { ?original sponge:scrapeRequested true }
  WHERE {
    ?format dcterms:isFormatOf ?original 
    FILTER NOT EXISTS {
     ?original sponge:scrapeRequested true .
    }
  }
---
name: mark_parts_part
update: |
  INSERT { ?part sponge:scrapeRequested true }
  WHERE {
    ?part dcterms:isPartOf ?whole 
    FILTER NOT EXISTS {
     ?part sponge:scrapeRequested true .
    }
  }
---
name: mark_parts_whole
update: |
  INSERT { ?whole sponge:scrapeRequested true }
  WHERE {
    ?part dcterms:isPartOf ?whole 
    FILTER NOT EXISTS {
     ?whole sponge:scrapeRequested true .
    }
  }
---
name: mark_redirects
update: |
  INSERT { ?b sponge:scrapeRequested true }
  WHERE {
    ?a http:redirectsTo ?b.
    ?a sponge:scrapeRequested true.
    FILTER NOT EXISTS {
     ?b sponge:scrapeRequested ?ts .
    }
  }
---
name: remove_xhtml_role
update: |
    DELETE { ?s xhtml:role ?o }
    WHERE {
      ?s xhtml:role ?o .
    }









