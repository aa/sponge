from django.db import models
from django.db.models import Q
from django.conf import settings

from ruamel.yaml import YAML
import re
from xml.etree import ElementTree as ET

from cssselect import HTMLTranslator
import elementpath

from .utils import querySelector, querySelectorAll, pluralize, etree_wrap
from .filters import *

from rdflib.namespace import Namespace, DC, DCTERMS
from rdflib import Graph, URIRef, Literal

from urllib.parse import urlparse, urlunparse
import requests
import hashlib, os
import uuid


DEFAULT_SCHEME = "http"

content_type_charset_pat = re.compile(r"(.+);\s*charset=(.+)")
def parse_content_type_charset (value):
    """ returns content_type, charset """
    m = content_type_charset_pat.search(value)
    if m is not None:
        m, c = m.groups()
        return m.lower(), c.lower()
    else:
        return value, ""

class MimeType (models.Model):
    class Meta:
        verbose_name = "MIME Types"
        verbose_name_plural = "MIME Types"
        ordering = ("mimetype",)

    mimetype = models.CharField(max_length=64, unique=True)
    extension = models.CharField(max_length=16, blank=True)
    description = models.TextField(blank=True)

    @classmethod
    def extension_for_mimetype(cls, mimetype, create=True):
        if create:
            ret, created = cls.objects.get_or_create(mimetype=mimetype, defaults={'extension': 'data'})
            return ret.extension
        else:
            try:
                return MimeType.objects.get(mimetype=mimetype).extension
            except cls.DoesNotExist:
                return

class URL (models.Model):
    class Meta:
        verbose_name = "URL"
        verbose_name_plural = "URLs"
        ordering = ("hostname", "path")
        constraints = [
            models.UniqueConstraint(fields=('scheme', 'hostname', 'port', 'params', 'path', 'query'), name='unique_url'),
        ]

    scheme = models.CharField(max_length=32)
    hostname = models.CharField(max_length=255)
    port = models.PositiveIntegerField(blank=True,null=True)
    params = models.CharField(max_length=255, blank=True)
    path = models.CharField(max_length=255, blank=True)
    query = models.CharField(max_length=255, blank=True)
    # query_parsed = models.JSONField(blank=True)
    # should this be here, or in response ?!
    # redirected_to = models.ForeignKey("URL", on_delete=models.CASCADE, related_name="redirections", blank=True, null=True)

    def __str__ (self):
        return self.url()

    # @classmethod
    # def parse (cls, url):
    #     p = urlparse(url)
    #     if not p.scheme:
    #         url = f"{DEFAULT_SCHEME}://{url}"
    #         p = urlparse(url)
    #     return cls(
    #         scheme = p.scheme,
    #         hostname = p.hostname,
    #         port = p.port,
    #         params = p.params,
    #         path = p.path,
    #         query = p.query
    #     )

    @classmethod
    def get_or_create (cls, url):
        p = urlparse(url)
        if not p.scheme:
            url = f"{DEFAULT_SCHEME}://{url}"
            p = urlparse(url)
        obj, created = cls.objects.get_or_create(
            scheme = p.scheme,
            hostname = p.hostname,
            port = p.port,
            params = p.params,
            path = p.path,
            query = p.query
        )
        return obj, created

    def url (self):
        netloc = self.hostname
        if self.port:
            netloc += ":{self.port}"
        return urlunparse((self.scheme, netloc, self.path, self.params, self.query, ''))

    def get_last_response (self):
        return Response.objects.filter(Q(url=self) | Q(redirected_from=self)).order_by("-date").first()

    def cget (self, request_headers=None):
        response = self.get_last_response()
        if response is not None:
            return response
        else:
            ret = self.get(request_headers)
            ret.save_contents()
            return ret

    def get (self, request_headers=None):
        """ returns: Response """
        url = self.url()
        response = Response()
        if request_headers is not None:
            r = requests.get(url, headers=request_headers, stream=True)
            response.request_headers = request_headers
        else:
            r = requests.get(url, stream=True)
            response.request_headers = {}
        response.status = r.status_code
        response.headers = dict(r.headers)
        if r.url != url:
            response.redirected_from = self
            response.url, _ = URL.get_or_create(r.url)
            url = r.url
        else:
            response.url = self
        if 'Content-Length' in r.headers:
            response.content_length = int(r.headers['Content-Length'])
        if 'Content-Type' in r.headers:
            response.content_type, response.encoding = parse_content_type_charset(r.headers['Content-Type'])
            if response.content_type == "text/html":
                response.content = PageContent.from_text(r.text)
            # SAVE RESPONSE AS FILE (in all cases)
            sha1 = hashlib.sha1()
            os.makedirs("tmp", exist_ok=True)
            code = uuid.uuid1()
            ext = MimeType.extension_for_mimetype(response.content_type) or "data"
            filename = os.path.join("tmp", f"{code}.{ext}")
            size = 0                    
            with open(filename, 'wb') as fd:
                for chunk in r.iter_content(chunk_size=128*1024):
                    fd.write(chunk)
                    sha1.update(chunk)
                    size += len(chunk)
            sha1 = sha1.hexdigest()
            # IF NEW ...
            newfilename = os.path.join(settings.MEDIA_ROOT, "files", sha1[:2], f"{sha1[2:]}.{ext}")
            filecontent, created = FileContent.objects.get_or_create(sha1=sha1, defaults={
                'file': os.path.relpath(newfilename, settings.MEDIA_ROOT),
                'extension': ext,
                'size': size })
            if created:
                os.makedirs(os.path.split(newfilename)[0], exist_ok=True)
                os.rename(filename, newfilename)
            response.file = filecontent
            response.save()
            # save to a file...
            # IF ...
            # content_type, content_length, URL -- internal or not?

        return response

class Response (models.Model):
    url = models.ForeignKey("URL", on_delete=models.CASCADE, related_name="responses")
    redirected_from = models.ForeignKey("URL", on_delete=models.CASCADE, related_name="redirections", blank=True, null=True)
    status = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now=True)
    request_headers = models.JSONField()
    # important headers here
    # server_date
    content_type = models.CharField(max_length=64)
    encoding = models.CharField(max_length=32, blank=True)
    content_length = models.PositiveIntegerField(null=True, blank=True)
    # content_length
    # headers = models.CharField(max_length=255)
    headers = models.JSONField()
    content = models.ForeignKey("PageContent", on_delete=models.CASCADE, related_name="responses", null=True, blank=True)
    file = models.ForeignKey("FileContent", on_delete=models.CASCADE, related_name="responses", null=True, blank=True)

    def save_contents (self):
        if self.content and not self.content.id:
            self.content.save()
        if self.url and not self.url.id:
            self.url.save()
        if self.redirected_from and not self.redirected_from.id:
            self.redirected_from.save()
        self.save()

    @property
    def content_type_with_encoding (self):
        if self.encoding:
            return f"{self.content_type}; charset={self.encoding}"
        else:
            return self.content_type


def githash_file(path, block_size=2**20):
    """ Should produce the same SHA1 hash git uses for a particular file """
    s = hashlib.sha1()
    filesize = os.path.getsize(path)
    s.update("blob {0}\0".format(filesize).encode("utf-8"))
    f = open(path, "rb")
    while True:
        data = f.read(block_size)
        if not data:
            break
        s.update(data)
    return s.hexdigest()

def githash_text(text, block_size=2**20):
    """ Should produce the same SHA1 hash git uses for a particular file """
    s = hashlib.sha1()
    s.update(f"blob {len(text)}\0".encode("utf-8"))
    s.update(text.encode("utf-8"))
    return s.hexdigest()

class PageContent (models.Model):
    # response = models.ForeignKey("Response", on_delete=models.CASCADE, related_name="pages")
    @classmethod
    def from_text (cls, text):
        sha1 = githash_text(text)
        ret, created = cls.objects.get_or_create(
            sha1=sha1,
            defaults={'text': text}
        )
        return ret

    sha1 = models.CharField(max_length=255, unique=True)
    encoding = models.CharField(max_length=32, default="utf-8")
    text = models.TextField()

    def parse_html_info (self):
        t = html5lib.parse(self.text, namespaceHTMLElements=False)
        # ctx['html_src'] = html_src
        localize_tree_filter(t, url)
        # ctx['html_src'] = innerHTML(t.find(".//body"))
        # ctx['tree'] = t
        ret = {}
        if t.tag == "html":
            html_elt = t
        else:
            html_elt = t.find(".//html")
        if 'lang' in html_elt.attrib:
            html_lang = html_elt.attrib['lang']
            ret['html_lang'] = html_lang

        # html title
        title_elt = html_elt.find(".//title")
        if title_elt is not None:
            ret['html_title'] = title_elt.text

        # html links
        links = ret['html_links'] = []
        for a_elt in t.findall(".//a[@href]"):
            a_href = urljoin(url_response.url, a_elt.attrib.get("href"))
            links.append(a_href)
        return ret

class FileContent (models.Model):
    # response = models.ForeignKey("Response", on_delete=models.CASCADE, related_name="files")
    sha1 = models.CharField(max_length=255)
    file = models.FileField(upload_to='files/%Y/%m/%d/')
    extension = models.CharField(max_length=16, blank=True)
    size = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)


####################################3#
####################################3#
####################################3#
####################################3#





SPONGE = Namespace("http://constantvzw.org/sponge/terms/")


class Namespace(models.Model):
    url = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=10, unique=True)

    def __str__ (self):
        return f"{self.name}: {self.url}"

class Scraper(models.Model):
    name = models.CharField(max_length=200)
    match = models.CharField(max_length=200)
    recompose = models.CharField(max_length=200)
    canonical = models.CharField(max_length=200)
    stopPropagationOnMatch = models.BooleanField(default=False)

    @classmethod
    def parse_yaml (cls, text):
        count = 0
        yaml=YAML()
        # for obj in yaml.load_all(text, Loader=yaml.SafeLoader):
        for obj in yaml.load_all(text):
            if obj:
                cls().init(**obj)
                count += 1
        return count
    
    def init(self,
                 match,
                 name='',
                 recompose=None,
                 canonical=None,
                 selectors=None,
                 append=None,
                 stopPropagationOnMatch=False):
        self.match = match
        self.name = name
        self.name = name
        self.canonical = canonical
        self.recompose = recompose
        # selectors are applied to the doc (elementtree) when html
        # the selectors are the adapters
        self.selectors = selectors
        # append: list of metadata to add (based on url_match)
        self.append = append
        # self._regex_matches_by_url = {}
        self.stopPropagationOnMatch = stopPropagationOnMatch


    def sniff (self, url):
        """
        returns ScraperMatch instance or None
        """

        # changed_p, url_which_may_be_normalized, graph_with_can_be_None
        # nb: if g is None, a new graph is created ONLY if matched
        # when g is given, this graph is modified and returned
        # # print (f"{self} sniff {url}")
        m = re.search(self.match, url)
        if m is not None:
            return ScraperMatch(self, url, m)
            # self._regex_matches_by_url[url] = m
            # if g is None:
            #     g = rdflib.Graph()
            # if self.recompose:
            #     nurl = re.sub(self.match, self.recompose, url)
            #     if str(nurl) != str(url):
            #         # print (f"NORMALIZE: {url}")
            #         g.add((URIRef(url), SPONGE['hasNormalizedForm'], URIRef(nurl)))
            #         url = nurl
            # self.add_sniff_metadata_to_graph (url, m, g)
            # return True, url, g
        # else:
        #     return False, url, g

    def __repr__ (self):
        return f"<Scraper: {self.name}>"

    def normalize_url(self, url):
        if self.recompose:
            nurl = re.sub(self.match, self.recompose, url)
            if nurl != url:
                return nurl

    def canonical_url(self, url):
        if self.canonical:
            turl = re.sub(self.match, self.canonical, url)
            if turl != url:
                return turl

class Selector (models.Model):
    class Meta:
            ordering = ['order']

    scraper = models.ForeignKey(Scraper, on_delete=models.CASCADE, related_name="selectors")
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    selector = models.CharField(max_length=200)
    typeof = models.CharField(max_length=200)
    property = models.CharField(max_length=200)
    filters = models.CharField(max_length=200)

subject_choices = (
    ("original", "original"),
    ("canonical", "canonical")
)

class Append (models.Model):
    class Meta:
            ordering = ['order']

    selector = models.ForeignKey(Selector, on_delete=models.CASCADE, related_name="appends")
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    property = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
    datatype = models.CharField(max_length=200)
    subject = models.CharField(max_length=200, choices=subject_choices, default="canonical")

class Wrap (models.Model):
    class Meta:
            ordering = ['order']

    selector = models.ForeignKey(Selector, on_delete=models.CASCADE, related_name="wraps")
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    tag = models.CharField(max_length=200)
    property = models.CharField(max_length=200)


class ScraperMatch (object):
    def __init__(self, scraper, url, match):
        self.scraper = scraper
        self.url = url
        self.original_url = url
        self.match = match
        self.g = Graph()

        nurl = scraper.normalize_url(url)
        if nurl:
            self.g.add((URIRef(url), SPONGE['normalized'], URIRef(nurl)))
            self.url = nurl
        
        self.canonical_url = scraper.canonical_url(url)
        if self.canonical_url and self.canonical_url != str(url): # 7 oct 2022: only add triple when urls differ
            # print (f"Adding triple for canonical {url} {self.canonical_url}")
            self.g.add((URIRef(url), SPONGE['canonical'], URIRef(self.canonical_url)))

        self.add_sniff_metadata_to_graph()

        # above: (normalized) url
        # above: graph g (from url "implicit" data)
        
    def qname (self, url):
        for pre, preurl in self.scraper.prefixes.items():
            if url.startswith(preurl):
                rest = url[len(preurl):]
                return f"{pre}:{rest}"
        return url
        
    def resolve_qname (self, qname):
        """ resolving a qname, seems like it should be part of rdflib, but where?
        todo: make part of a general namespaces collection class ?!
        """
        if ":" in qname:
            pre, post = qname.split(":", 1)
            if pre in self.scraper.prefixes:
                # return URIRef(self.prefixes[pre] + post)
                return Namespace(self.scraper.prefixes[pre])[post]
            # elif pre.lower() == "xsd":
            #     return rdflib.namespace.XSD[post]
            else:
                raise Exception(f"Unknown prefix {pre}")
        return URIRef(qname)                          

    def apply_prefixes (self, t):
        use_p = {}
        for elt in elementpath.select(t, HTMLTranslator().css_to_xpath('html')):
            # print (f"found html element {elt}")
            if 'prefix' in elt.attrib:
                use_p.update(parse_prefix(elt.attrib['prefix']))
            use_p.update(self.scraper.prefixes)
            new_value = " ".join([f"{key}: {value}" for key, value in use_p.items()])
            elt.attrib['prefix'] = new_value
            # print (f"setting html.prefix {new_value}")
            return # assume one html element

    def resolve_value_backref (self, value):
        # Allow value to have backrefs... and be conditional if so
        matched = False
        has_backref = False
        url_match_dict = self.match.groupdict() # or just get the match dynamically?
        def _sub (m):
            has_backref = True
            # NEED URL_MATCH_DICT
            name = m.group("name")
            if name in url_match_dict:
                matched = True
                return url_match_dict[name]
            else:
                return ''
        return re.sub(r"\\g<(?P<name>\w+)>", _sub, value), has_backref and matched

    def add_sniff_metadata_to_graph (self):
        """
        nb: if value is blank, triple will not be added (permitting a certain kind of conditionality)
        """
        if self.scraper.append is not None:
            for pvpair in pluralize(self.scraper.append):    
                if 'value' in pvpair:
                    value, _ = self.resolve_value_backref(pvpair['value'])
                    if value:
                        subject = pvpair.get('subject')
                        # subject can be: none/default, original, canonical
                        if subject == "original":
                            subject = URIRef(self.url)
                        elif self.canonical_url:
                            subject = URIRef(self.canonical_url)
                        else:
                            subject = URIRef(self.url)
                        
                        propname = pvpair.get("property")
                        prop = self.resolve_qname(propname)
                        
                        datatype = pvpair.get('datatype')
                        if datatype:
                            datatype=self.resolve_qname(datatype)
                            value = Literal(value, datatype=datatype)
                        else:
                            value = Literal(value)
                            
                        self.g.add((subject, prop, value))

    # def add_sniff_metadata_to_tree (self, t):
    #     if self.scraper.append is not None:
    #         for pvpair in pluralize(self.scraper.append):
    #             pname = self.resolve_qname(pvpair['property'])
    #             # EXTRACT THE DATA FROM THE GRAPH AND stick it in tree
    #             for obj in self.g.objects(subject=URIRef(self.url), predicate=URIRef(pname)):
    #                 self.append_arbitrary_metadata_to_body(t, pvpair['property'], obj.value, obj.datatype)

    def append_arbitrary_metadata_to_body (self, t, p, o, s=None):
        """ todo: allow optional subject -- use resource/about property """
        # print (f"append_arbitrary_metadata_to_body t:{t}, name:{name}, value:{value}, datatype:{datatype}")
        metaroot = t.find(".//div[@id='ds_arbitrarymetadata']")
        if metaroot is None:
            body = t.find(".//body")
            metaroot = ET.SubElement(body, "div", id="ds_arbitrarymetadata")
        # NEED TO REVERSE THE PROPERTY NAME BASED ON BINDINGS
        attrs = {}
        if isinstance(o, URIRef):
            use_tag = "link"
            attrs['href'] = f"{o}"
        else:
            use_tag = "span"
            if o.datatype:
                attrs['datatype'] = f"{o.datatype}"
            attrs['content'] = f"{o}"
        if s is not None:
            attrs['about'] = f"{s}"
        attrs['property'] = self.qname(p)
        elt = ET.SubElement(metaroot, use_tag, **attrs)

    def add_sniff_metadata_to_tree (self, t):
        """
        Puts all triples from local graph into tree (allowing for variable subjects)
        """
        # TODO: ADD CANONICAL AS about/resource of html tag....
        if self.canonical_url:
            if t.tag == "html":
                html_tag = t
            else:
                html_tag = t.find("./html")
            if html_tag is not None:
                assert 'resource' not in html_tag
                html_tag.attrib['resource'] = self.canonical_url
            
        for s, p, o in self.g:
            self.append_arbitrary_metadata_to_body(t, s=s, p=p, o=o)
        # if self.scraper.append is not None:
        #     for pvpair in pluralize(self.scraper.append):
        #         pname = self.resolve_qname(pvpair['property'])
        #         # EXTRACT THE DATA FROM THE GRAPH AND stick it in tree
        #         for obj in self.g.objects(subject=URIRef(self.url), predicate=URIRef(pname)):
        #             self.append_arbitrary_metadata_to_body(t, pvpair['property'], obj.value, obj.datatype)

    # def append_arbitrary_metadata_to_body (self, t, name, value, datatype):
    #     """ todo: allow optional subject -- use resource/about property """
    #     # print (f"append_arbitrary_metadata_to_body t:{t}, name:{name}, value:{value}, datatype:{datatype}")
    #     metaroot = t.find(".//div[@id='ds_arbitrarymetadata']")
    #     if metaroot is None:
    #         body = t.find(".//body")
    #         metaroot = ET.SubElement(body, "div", id="ds_arbitrarymetadata")
    #     if datatype:
    #         elt = ET.SubElement(metaroot, "span", property=name, content=str(value), datatype=datatype)
    #     else:
    #         elt = ET.SubElement(metaroot, "span", property=name, content=str(value))    
        
    def apply_selectors(self, t, selectors=None):
        for s in selectors or self.scraper.selectors:
            for elt in querySelectorAll(t, s['selector']):
                if 'wrap' in s:
                    for wrap in pluralize(s['wrap']):
                        etree_wrap(elt, wrap['tag'], property=wrap['property'])
                    
                if 'property' in s:
                    elt.attrib['property'] = s['property']
                if 'datatype' in s:
                    elt.attrib['datatype'] = s['datatype']
                if 'typeof' in s:
                    elt.attrib['typeof'] = s['typeof']
                if 'filters' in s:
                    for filter in s['filters'].split("|"):
                        vf = get_value_filter(filter)
                        if vf:
                            vf.filter(elt)
                        else:
                            print (f"Warning, unknown value filter {filter}")
                        # if filter == "autolink":
                        #     autolink_filter(elt)
                        # elif filter == "datetime":
                        #     datetime_filter(elt)
                        # elif filter == "constant_converser":
                        #     constant_converser_filter(elt)
                if 'append' in s:
                    # for p, v, dt in self.metadata_from_url(url_match, s['append']):
                    #     if dt:
                    #         ET.SubElement(elt, "span", property=p, datatype=dt, content=value)
                    #     else:
                    #         ET.SubElement(elt, "span", property=p, content=value)
                            
                    for pvpair in pluralize(s['append']):    
                        if 'value' in pvpair:
                            # value = self.resolve_value_backref(url_match, pvpair['value'])
                            value = pvpair['value']
                            if 'datatype' in pvpair:
                                ET.SubElement(elt, "span", property=pvpair['property'], datatype=pvpair['datatype'], content=value)
                            else:
                                ET.SubElement(elt, "span", property=pvpair['property'], content=value)
                        elif 'href' in pvpair:
                            ET.SubElement(elt, "a", property=pvpair['property'], href=pvpair['href'])
                if 'children' in s:
                    self.apply_selectors(elt, s['children'])

    def apply_scraper (self, t):
        if self.scraper.prefixes:
            self.apply_prefixes(t)
        self.add_sniff_metadata_to_tree(t)
        if self.scraper.selectors:
            self.apply_selectors(t)
