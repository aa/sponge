from django.urls import path

from . import views

urlpatterns = [
    path('b/', views.browse, name='browse'),
    path('b/<path:path>', views.browse, name='browse'),
    path('c/<path:path>', views.content, name='content'),
    path('', views.responses, name='index'),
]