Sponge continues
========================

February 2023, incorporating experiments with yaml defined scrapers into a django framework to better test / develop them in the context of an interactive "browser"-style application.

Questions: Does making scrapers (user) editable make for an interesting/accessible extensibility.

Plan: use [Admin Sortable](https://django-admin-sortable2.readthedocs.io/en/latest/usage.html#usage)






