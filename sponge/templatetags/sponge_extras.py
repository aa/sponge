from django import template
from urllib.parse import urlencode

register = template.Library()

@register.simple_tag
def urlencoder(d, **kwargs):
    use = d.copy()
    for key, value in kwargs.items():
        if value is None:
            if key in use:
                del use[key]
        else:
            use[key] = value
    return urlencode(use)
