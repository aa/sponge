# Generated by Django 4.1.7 on 2023-03-02 21:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sponge', '0014_alter_pagecontent_sha1'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='url',
            unique_together=set(),
        ),
        migrations.AddConstraint(
            model_name='url',
            constraint=models.UniqueConstraint(models.F('scheme'), models.F('hostname'), models.F('port'), models.F('params'), models.F('path'), models.F('query'), name='unique_url'),
        ),
    ]
