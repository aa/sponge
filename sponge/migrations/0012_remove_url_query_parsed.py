# Generated by Django 4.1.7 on 2023-03-02 20:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sponge', '0011_rename_body_pagecontent_text'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='url',
            name='query_parsed',
        ),
    ]
